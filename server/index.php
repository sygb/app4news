<?php

include_once 'global.php';

$c = IO::I('c', 'index');
$a = IO::I('a', 'main');

$_RG['PG_C'] = $c;
$_RG['PG_A'] = $a;

$_RG['PG_GET'] = $_GET;

if(lx_logic($c, $a) === false){
    die('Not Found!');
}