<?php
/**
 * 全局配置
 */
#############################################
# MySQL服务器、用户名、密码
define('SQL_SVR', '');    
define('SQL_USR', '');        
define('SQL_PWD', '');    
define('SQL_DB', '');    

define('MD5_SALT', '');

# 是否强制所有PHP类都只位于class目录下
define('FORCE_AUTOLOAD', false);
# 类文件路径
define('RUNTIME_DIR_CLASS',  'class/');
# 方法路径
define('RUNTIME_DIR_METHOD', 'method/');
# DATA路径
define('RUNTIME_DIR_DATA', 'data/');
# 找不到方法时指定默认处理方法
define('RUNTIME_METHOD_NOTFOUND', 'method/lx/notfound.php');

#############################################

# TPL
define('TPL_DEFAULT_TITLE', 'EatWhat');
$TPL_DEFAULT_JS = array(
    
);
$TPL_DEFAULT_CSS = array(
    
);

$LANGS        = array(
    'method'   => '方法',
    'username' => '用户名',
    'password' => '密码'
);