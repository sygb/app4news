<?php


$channel_list = Channel::parse(IO::I('channel'));

if($channel_list === FALSE){
    IO::E(-1, 'Channel Parse Failed.');
}else{
    IO::O(array(
        'list' => $channel_list
    ));
}