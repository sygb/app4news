<?php

class Data{
    public static function read($filename){
        $filename = RUNTIME_DIR_DATA.$filename;
        if(file_exists($filename)){
            return file_get_contents($filename);
        }else{
            return FALSE;
        }
    }
    
    public static function read_json($filename, $assoc = true){
        $raw = self::read($filename);
        if($raw === FALSE){
            return FALSE;
        }
        return json_decode($raw, $assoc);
    }
    
    public static function write($filename, $content, $overwrite = TRUE){
        $filename = RUNTIME_DIR_DATA.$filename;
        if(!$overwrite && file_exists($filename)){
            return FALSE;
        }else{
            return file_put_contents($filename, $content);
        }
    }
    
    public static function write_json($filename, $content, $overwrite = TRUE){
        return self::write($filename, json_encode($content), $overwrite);
    }
}