define(['config', 'yago'], function(config, Yago){
    
    function mkdiv(className){
        var d = document.createElement('div');
        d.className = className;
        return d;
    }
    
    
    var textlist = {
        build : function(data, layout_height){
            var d = document.createElement('div');
            d.className = 'apf-textlist';
            d.style.top = layout_height + 'px';
            d.style.height = config.SIZE.TEXTLIST_ITEM_HEIGHT * data.items.length + 'px';
            d.style.width = Yago.scrH + 'px';
            layout_height += config.SIZE.TEXTLIST_ITEM_HEIGHT * data.items.length;
            var i, elem, item, text, date;
            var w = Yago.scrW - config.SIZE.TEXTLIST_ITEM_LEFT * 2;
            for(i = 0; i < data.items.length; i ++){
                item = data.items[i];
                elem = mkdiv('apf-textlist-item');
                text = mkdiv('apf-textlist-item-text');
                date = mkdiv('apf-textlist-item-date');
                
                elem.appendChild(text);
                elem.appendChild(date);
                
                text.innerHTML = item.title;
                date.innerHTML = item.date;
                
                text.style.width = w + 'px';
                date.style.width = w + 'px';
                                
                Yago.css.moveY(elem, config.SIZE.TEXTLIST_ITEM_HEIGHT * i);
                d.appendChild(elem);
            }
            
            return {
                element : d,
                layout_height : layout_height
            };
        }
    }
    
    return textlist;
    
});