define(['config', 'yago'], function(config, Yago){
    
    function mkdiv(className){
        var d = document.createElement('div');
        d.className = className;
        return d;
    }
    
    var picset = {
        build : function(data, layout_height){
            var d = mkdiv('apf-picset');
            var c = mkdiv('apf-picset-ct');
            var t = mkdiv('apf-picset-tt');
            var v = [], vc = 0;
            var w = Yago.scrW;
            var l = 0;
            var h = parseInt(data.hw * Yago.scrW);
            d.style.width  = w + 'px';
            d.style.height = h + config.SIZE.PICSET_TITLE_HEIGHT + 'px';
            d.appendChild(c);
            d.appendChild(t);
            d.style.top    = layout_height + 'px';
            layout_height += config.SIZE.PICSET_TITLE_HEIGHT + h;
            c.style.width  = w * 3 + 'px';
            c.style.height = h + 'px';
            t.style.width  = w + 'px';
            data.items.forEach(function(i){
                var img = new Image();
                var dot = mkdiv('apf-picset-vn');
                dot.style.right  = config.SIZE.PICSET_DOT_RIGHT + l * config.SIZE.PICSET_DOT_WIDTH + 'px';
                img.src = i.img;
                img.style.width  = w + 'px';
                img.style.height = h + 'px';
                img.style.left   = l * w + 'px';
                img.className    = 'apf-picset-img';
                v.push(dot);
                c.appendChild(img);
                d.appendChild(dot);
                
                l ++;
            });
            t.style.width  = w - config.SIZE.PICSET_DOT_RIGHT - config.SIZE.PICSET_DOT_WIDTH * l + 'px';
            v.reverse();
            Yago.touch.enableCarousel(c, 'x', {
                step : w,
                preventDefault : true,
                speed : 20,
                carouselEndCallback : function(n){
                    t.innerHTML = data.items[n].title;
                    v[vc].className = 'apf-picset-vn';
                    v[n].className  = 'apf-picset-vn cur';
                    vc = n;
                }
            });
            t.innerHTML = data.items[0].title;
            v[0].className  = 'apf-picset-vn cur';

            return {
                element : d,
                layout_height : layout_height,
                callback : function(){
                    c.mv.reWrap();
                }
            };
        }
    };
    return picset;
});