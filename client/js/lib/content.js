define(
    _contents.map(
        function(x){
            return 'content_' + x;
        }
    ), 
    function(){
        var content = {
            
            contains : {},
            check : function(x){
                return !!content.contains[x];
            },
            build : function(type, data, layout_height){
                return content.contains[type].build(data, layout_height);
            }
        };
        var i;
        for(i = 0; i < _contents.length; i ++){
            content.contains[_contents[i]] = arguments[i];
        }
        return content;
    }
);