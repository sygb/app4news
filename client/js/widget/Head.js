define(['config', 'yago'], function(config, Yago){
    
    var Head = {
        init : function(d){
            d.style.width = Yago.scrW + 'px';
        }
    };
    
    return Head;
    
});