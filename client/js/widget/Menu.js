define(['config', 'yago'], function(config, Yago){
        
    var W_2D3 = parseInt(Yago.scrW / 3 * 2);
    var W_1D3 = Yago.scrW - W_2D3;
    var ttAName = Yago.transitionAttrName;
    var tfAName = Yago.transformAttrName;
    
    var a4n;
    
    var Menu = {
        
        init : function(d){
            d.style.width = W_2D3 + 'px';
        },
        
        show : function(n, option){
            option = option || {};
            var default_option = {
                mask : false,
                maskcallback : null,
                time : 300,
                transition : 'ease-out'
            };
            var i;
            for(i in default_option){
                if(!(i in option)){
                    option[i] = default_option[i];
                }
            }
            var pos_r = n.className.indexOf('right') >= 0;
            n.style[ttAName] = '';
            if(pos_r){
                n.style[tfAName] = 'translateX(' + W_2D3 + 'px)';
            }else{
                n.style[tfAName] = 'translateX(-' + W_2D3 + 'px)';
            }
            if(option.mask){
                a4n.show(option.mask, option)
            }
            n.style[ttAName] = 'all ' + option.transition + ' ' + option.time + 'ms';
            n.style.display  = 'block';
            setTimeout(function(){
                n.style[tfAName] = '';
                setTimeout(function(){
                    n.style[ttAName] = '';
                }, option.time);
            }, 10);
        },
        
        hide : function(n, option){
            option = option || {};
            var default_option = {
                mask : false,
                maskcallback : null,
                time : 300,
                transition : 'ease-out'
            };
            var i;
            for(i in default_option){
                if(!(i in option)){
                    option[i] = default_option[i];
                }
            }
            var pos_r = n.className.indexOf('right') >= 0;
            n.style[tfAName] = '';
            n.style.display  = 'block';
            n.style[ttAName] = 'all ' + option.transition + ' ' + option.time + 'ms';
            if(option.mask){
                a4n.hide(option.mask, option)
            }
            setTimeout(function(){
                if(pos_r){
                    n.style[tfAName] = 'translateX(' + W_2D3 + 'px)';
                }else{
                    n.style[tfAName] = 'translateX(-' + W_2D3 + 'px)';
                }
                setTimeout(function(){
                    n.style[tfAName] = '';
                    n.style.display  = 'none';
                }, option.time);
            }, 10);
        },
        
        ready : function(a){
            a4n = a;
        }
    };
    
    return Menu;
    
});