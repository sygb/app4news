define(['config', 'yago'], function(config, Yago){
    var View = {
        
        init : function(d){
            d.style.width  = Yago.scrW + 'px';
            d.style.height = Yago.scrH + 'px';
            d.apfNeedUpdate = [];
            d.apfUpdate = function(){
                d.apfNeedUpdate.forEach(function(fx){
                    fx();
                });
            }
        },
        
        show : function(n, option){
            option = option || {};
            var default_option = {
                route : 'none',
                time : 300,
                transition : 'ease-in'
            };
            var i;
            for(i in default_option){
                if(!(i in option)){
                    option[i] = default_option[i];
                }
            }
            switch(option.route){
                default:
                    n.style.display = 'block';
                    n.apfUpdate();
                    break;
            }
        },
        
        hide : function(n, option){
            option = option || {};
            var default_option = {
                route : 'none',
                time : 300,
                transition : 'ease-out'
            };
            var i;
            for(i in default_option){
                if(!(i in option)){
                    option[i] = default_option[i];
                }
            }
            switch(option.route){
                default:
                    n.style.display = 'none';
                    break;
            }
        }
    }
    
    return View;
});