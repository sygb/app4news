define(['config', 'yago'], function(config, Yago){
    
    var W_2D3 = parseInt(Yago.scrW / 3 * 2);
    
    var MenuItem = {
        init : function(d){
            d.style.width = W_2D3 + 'px';
        }
    };
    
    return MenuItem;
});