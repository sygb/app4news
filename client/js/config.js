define(function(){
    return {
        URL  : {
            // 服务端URL
    //        SVR_URL : 'http://219.135.157.253/app4news/server/'
            SVR_URL : 'http://192.168.2.1/a4n/server/'
    //        SVR_URL : 'http://192.168.1.123/a4n/server/'
        },
        SIZE : {
            //
            HEAD_HEIGHT : 45,
            //
            CHANNEL_HEIGHT_ALL : 36,
            // 对应 .apf-channel-item 的 width
            CHANNEL_WIDTH : 96, 
            // 对应 .apf-channel-scroll-under 的 left
            CHANNEL_LEFT  : 4,  
            //
            CHANNEL_SORT_LABEL_HEIGHT :36,
            //
            CHANNEL_SORT_ITEM_WIDTH : 66,
            //
            CHANNEL_SORT_ITEM_HEIGHT : 40,
            //
            CHANNEL_SORT_ITEM_PER_LINE : 4,

            // 对应 .apf-picset-tt 的 height
            PICSET_TITLE_HEIGHT : 36,
            // 图集右下角圆点到页面右边缘的距离
            PICSET_DOT_RIGHT : 8,
            // 图集右下角圆点占据的总宽度
            PICSET_DOT_WIDTH : 12,

            // 对应 .apf-textlist-item 的 outerHeight
            TEXTLIST_ITEM_HEIGHT : 61,
            // 对应 .apf-textlist-item-text 的 left
            TEXTLIST_ITEM_LEFT : 12,

            // 下拉刷新阈值
            DROP_REFRESH_THRESHOLD : 60,
            // 下拉刷新条高度
            DROP_REFRESH_HEIGHT : 40,
            // 下拉刷新条宽度
            DROP_REFRESH_WIDTH  : 160,
            // 下拉刷新图标精灵图步进
            DROP_REFRESH_STEP   : 40
        },
        TEXT : {
            CHANNEL_SORT_LABEL   : '拖动以管理频道',

            DROP_REFRESH_DROPING : '继续下拉将刷新',
            DROP_REFRESH_WAIT_RE : '释放以刷新',
            DROP_REFRESH_ING     : '正在刷新……'
        }
    }
});